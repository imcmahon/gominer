package spi


import (
	"os"
	"syscall"
	"fmt"
	"log"
)

const (
	CONTROL_MODULE_START_ADDR   int64 = 0x44E10000
	CONTROL_MODULE_END_ADDR  	int64 = 0x44E11FFF
	CONTROL_MODULE_SIZE   		int = 0x2000

	GPIO_SIZE          uint32 = 0x2000
	GPIO0_START_ADDR   uint32 = 0x44E07000
	GPIO1_START_ADDR   uint32 = 0x4804C000
	GPIO2_START_ADDR   uint32 = 0x481AC000
	GPIO3_START_ADDR   uint32 = 0x481AE000

	GPIO_OE            uint32 = 0x134
	GPIO_SETDATAOUT    uint32 = 0x194
	GPIO_CLEARDATAOUT  uint32 = 0x190
	GPIO_DATAIN        uint32 = 0x138
)

type BbbHw struct {
	control_module 	[]byte
	mosi_gpio		[]byte
	miso_gpio		[]byte
	sck_gpio		[]byte
}


func BBB() (*BbbHw, error) {
	hw := new(BbbHw)

	file, err := os.OpenFile("/dev/mem", syscall.O_RDWR|syscall.O_SYNC, 0666)
	if err != nil {
		return hw, fmt.Errorf("unable to open /dev/mem: %v", err)
	}

	hw.control_module, err = syscall.Mmap(	int(file.Fd()), 
											CONTROL_MODULE_START_ADDR, 
											CONTROL_MODULE_SIZE, 
											syscall.PROT_READ | syscall.PROT_WRITE, 
											syscall.MAP_SHARED)
	if err != nil {
		return hw, fmt.Errorf("error mmaping /dev/mem: %v", err)
	}

	return hw, err
}


func (hw *BbbHw) reset() {
	log.Printf("resetting BBB")
}