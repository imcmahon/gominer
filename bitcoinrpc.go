package main

import (
	"fmt"
	"bytes"
	"encoding/hex"
	"encoding/binary"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"errors"
)

type BitcoinRPCHost struct {
	host string
	port int
	user, password string
}

func (host *BitcoinRPCHost)call(method string, params []interface{}) (map[string]interface{}, error) {

	data, err := json.Marshal(map[string]interface{}{
		"method": 	method,
		"id":		1,
		"params":	params,
	})

	if err != nil {
		log.Fatalf("Marshal: %v", err)
		return nil, err
	}

	url := fmt.Sprintf("http://%s:%s@%s:%d", host.user, host.password, host.host, host.port)

	resp, err := http.Post(	url,
							"application/json",
							strings.NewReader(string(data)))
	if err != nil {
		log.Printf("Post: %v", err)
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("ReadAll: %v", err)
		return nil, err
	}

	result := make(map[string]interface{})
	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Printf("Unmarshal: %v", err)
		return nil, err
	}

	return result, nil
}

/**
	returns midstate, data, err
*/
func (host *BitcoinRPCHost)getwork() ([]uint32, []uint32, error) {
	params := make([]interface{}, 0)
	resp, err := host.call("getwork", params)
	if err != nil {
		log.Printf("Getwork: %v", err)
		return nil, nil, err
	}

	if resp["error"] != nil {
		log.Printf("response: %s\n", resp)
		log.Printf("error: %s\n", (resp["error"].(map[string]interface{}))["message"])
		return nil, nil, errors.New((resp["error"].(map[string]interface{}))["message"].(string))
	}

	result := resp["result"].(map[string]interface{})

/*
	result["midstate"] = "DEADBEEF"

	result["data"] = "01000000" +
    "81cd02ab7e569e8bcd9317e2fe99f2de44d49ab2b8851ba4a308000000000000" +
    "e320b6c2fffc8d750423db8b1eb942ae710e951ed797f7affc8892b0f1fc122b" +
    "c7f5d74d" +
    "f2b9441a" +
     "42a14695"
     */

    //log.Printf("target: %s\n", result["target"])

	midstate,err := str_to_uint32_array(result["midstate"].(string))
	if err != nil {
		log.Printf("string to []uint32 failed: %v", err)
		return nil, nil, err
	}

	data,err := str_to_uint32_array(result["data"].(string))
	if err != nil {
		log.Printf("string to []uint32 failed: %v", err)
		return nil, nil, err
	}

	return midstate, data, nil
}


func str_to_uint32_array(data_s string) ([]uint32, error) {
	bytearray, err := hex.DecodeString(data_s)
	if err != nil {
		log.Printf("decodestring: %v", err)
		return nil, err
	}

	byte_length := len(data_s) / 2
	word_length := byte_length / 4

	data := make([]uint32, word_length)

	for x,i:=0,0; x<len(bytearray); x+=4 {
		var word uint32
		var wordarray []byte = bytearray[x:x+4]

		buf := bytes.NewBuffer(wordarray)
		err := binary.Read(buf, binary.LittleEndian, &word)

		if err != nil {
			log.Printf("binary.Read: %v", err)
			return nil, err
		}

		data[i] = word
		i++
	}

	return data, nil
}
