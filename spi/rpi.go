package spi


import (
	"syscall"
	"os"
	"fmt"
	"reflect"
	"unsafe"
	"log"
	"time"
)

const (
	RPI_GPIO_BASE	int64 = 0x20200000
	BLOCK_SIZE		int   = (4*1024)
)

type RpiHw struct {
	gpio []uint32
	spifile *os.File
}


func RPI() (*RpiHw, error) {
	hw := new(RpiHw)

	file, err := os.OpenFile("/dev/mem", syscall.O_RDWR|syscall.O_SYNC, 0666)
	if err != nil {
		return hw, fmt.Errorf("unable to open /dev/mem: %v", err)
	}

	gpio_bytes, err := syscall.Mmap(int(file.Fd()), 
									RPI_GPIO_BASE, 
									BLOCK_SIZE,
									syscall.PROT_READ | syscall.PROT_WRITE, 
									syscall.MAP_SHARED)

	header := *(*reflect.SliceHeader)(unsafe.Pointer(&gpio_bytes))
	header.Len /= 4
	header.Cap /= 4
	hw.gpio = *(*[]uint32)(unsafe.Pointer(&header))

	file.Close()

	hw.spifile, err = os.OpenFile("/dev/spidev0.0", syscall.O_RDWR, 0666)
	if err != nil {
		return hw, fmt.Errorf("unable to open /dev/spidev0.0: %v", err)
	}

	var mode int = 0
	var bits int = 8
	var speed int = 1000000

	log.Printf("asserting spi ioctls\n")
	if ioctl(int(hw.spifile.Fd()), SPI_IOC_WR_MODE, uintptr(unsafe.Pointer(&mode))) != nil { return hw, fmt.Errorf("Unable to set WR MODE: %v", err) }
	if ioctl(int(hw.spifile.Fd()), SPI_IOC_RD_MODE, uintptr(unsafe.Pointer(&mode))) != nil { return hw, fmt.Errorf("Unable to set RD MODE: %v", err) }
	if ioctl(int(hw.spifile.Fd()), SPI_IOC_WR_BITS_PER_WORD, uintptr(unsafe.Pointer(&bits))) != nil { return hw, fmt.Errorf("Unable to set WR BITS: %v", err) }
	if ioctl(int(hw.spifile.Fd()), SPI_IOC_RD_BITS_PER_WORD, uintptr(unsafe.Pointer(&bits))) != nil { return hw, fmt.Errorf("Unable to set RD BITS: %v", err) }
	if ioctl(int(hw.spifile.Fd()), SPI_IOC_WR_MAX_SPEED_HZ, uintptr(unsafe.Pointer(&speed))) != nil { return hw, fmt.Errorf("Unable to set WR SPEED: %v", err) }
	if ioctl(int(hw.spifile.Fd()), SPI_IOC_RD_MAX_SPEED_HZ, uintptr(unsafe.Pointer(&speed))) != nil { return hw, fmt.Errorf("Unable to set RD SPEED: %v", err) }

	return hw, nil
}

func (hw *RpiHw) inp_gpio(g uint32) {
	hw.gpio[g/10] &= ^(7<<(((g)%10)*3))
}

func (hw *RpiHw) out_gpio(g uint32) {
	hw.gpio[g/10] |= (1<<(((g)%10)*3))
}

func (hw *RpiHw) set_gpio_alt(g, a uint32) {
	var b uint32

	if a <= 3 {
		b = a + 4
	} else if a == 4 {
		b = 3
	} else {
		b = 2
	}

	hw.gpio[g/10] |= b << (((g)%10)*3)
}

func (hw *RpiHw) reset() {
	// TODO: reset BANKS

	fmt.Println("in rpi reset")

	hw.inp_gpio(10); hw.out_gpio(10);

	hw.inp_gpio(10); hw.out_gpio(10)
	hw.inp_gpio(11); hw.out_gpio(11)

	hw.gpio[7] = 1<<11 // set sck
	for i := 0; i < 1000000; i++ {
		hw.gpio[7] = 1 << 10 // mosi set
		time.Sleep(time.Microsecond)
		hw.gpio[10] = 1 << 10 // mosi clr
		time.Sleep(time.Microsecond)
	}
	hw.gpio[10] = 1 << 11 // clear sck

	hw.inp_gpio(11)
	hw.inp_gpio(10)
	hw.inp_gpio(9)
	hw.set_gpio_alt(11, 0)
	hw.set_gpio_alt(10, 0)
	hw.set_gpio_alt(9, 0)
}
