package spi

import (
	"syscall"
	"os"
)

const (
	SPI_IOC_WR_MODE 			uintptr = 0x40016b01
	SPI_IOC_RD_MODE 			uintptr = 0x80016b01
	SPI_IOC_WR_BITS_PER_WORD 	uintptr = 0x40016b03
	SPI_IOC_RD_BITS_PER_WORD 	uintptr = 0x80016b03
	SPI_IOC_WR_MAX_SPEED_HZ 	uintptr = 0x40046b04
	SPI_IOC_RD_MAX_SPEED_HZ 	uintptr = 0x80046b04
)


func ioctl(fd int, request, argp uintptr) error {
	_, _, errorp := syscall.Syscall(syscall.SYS_IOCTL, uintptr(fd), request, argp)
	if errorp == 0 { return nil }
	return os.NewSyscallError("ioctl", errorp)
}
