package spi

import (
	"log"
)

type DummyHw struct {

}

func Dummy() (*DummyHw, error) {
	hw := new(DummyHw)

	log.Printf("Constructing Dummy Hardware")

	return hw, nil
}

func (hw *DummyHw) reset() {
	log.Printf("DummyHw resetting\n")
}