package main 

import (
	"log"
	"flag"
	"bitbucket.org/imcmahon/gominer/spi"
)

var rpchost BitcoinRPCHost

var plat_rpi bool
var plat_bbb bool

func main() {
	rpchost = BitcoinRPCHost{"us.eclipsemc.com", 8337, "ssi_bitfury1", "a"}
	//rpchost = BitcoinRPCHost{"miner", 18332, "bitcoinrpc", "HpcJGP3Ait2ZSW3Wz5aDrJHRLx4xzUAzDv33hUUq7hJs"}

	log.Println("miner starting...")

	flag.BoolVar(&plat_rpi, "rpi", false, "Platform RaspberryPi")
	flag.BoolVar(&plat_bbb, "bbb", false, "Platform BeagleBone Black")

	flag.Parse()

	var my_spi *spi.Spi
	var err error

	if plat_rpi {
		log.Println("Hello, RPi")

		my_spi,err = spi.NewRPI()

		if err != nil {
			log.Fatalf("Error opening spi: %v\n", err)
		}
	} else if plat_bbb {
		log.Println("Hello, BBB")
	} else {
		log.Println("Oh, you must be running me on a platform where I won't do anything useful")
		my_spi, err = spi.NewDummy()
		log.Printf("spi: %s\n", my_spi)

		if err != nil {
			log.Fatalf("Error opening spi: %v\n", err)
		}
	}


	my_spi.Reset()



	/*
	for ;; {
		cpu_miner()
	}
	*/



	// chainminer pseudo
	
	// get_start // starts a getwork thread
	// put_start // starts a putwork thread
	// chip_init

	// chips = spi_start(chipconf, chipfast)
	// print chips detected

	// for ;;
	//		cpu_miner() as a test
	//		spi_miner(chips, chipconf, chipfast)
	//		chip_stat(chips)

	// spi_close

}
