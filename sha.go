package main

import (
	"bytes"
	"encoding/binary"
	"bitbucket.org/imcmahon/gominer/sha256"
	//"encoding/hex"
)


func as_byte_array(data []uint32, order binary.ByteOrder) ([]byte, error) {
	buf := new(bytes.Buffer)

	for _, v := range data {
		err := binary.Write(buf, order, v)
		if err != nil { return nil, err }
	}

	return buf.Bytes(), nil
}

func as_uint32_slice(b []byte, order binary.ByteOrder) []uint32 {
	out := make([]uint32, len(b)/4)

	for i,j:=0,0; i<len(b); i+=4 {
		out[j] = order.Uint32(b[i:i+4])
		j++
	}

	return out
}

func sha256_go(data []uint32) ([]uint32, error) {
	bytes, err := as_byte_array(data, binary.BigEndian)
	//log.Printf("sha256_go input: %.8x\n", bytes)
	//log.Printf("sha256_go input: %s\n", hex.EncodeToString(bytes))
	if err != nil { return nil, err }

	hash := sha256.New()
	hash.Write(bytes)

	return as_uint32_slice(hash.Sum(nil), binary.BigEndian), nil
}

func sha256_go_unpadded(data []uint32) ([]uint32, error) {
	bytes, err := as_byte_array(data, binary.BigEndian)
	//log.Printf("sha256_go input: %.8x\n", bytes)
	//log.Printf("sha256_go input: %s\n", hex.EncodeToString(bytes))
	if err != nil { return nil, err }

	hash := sha256.NewUnpadded()
	hash.Write(bytes)

	return as_uint32_slice(hash.Sum(nil), binary.BigEndian), nil
}

func sha256_from_midstate(data []uint32, midstate []uint32) ([]uint32, error) {
	bytes, err := as_byte_array(data, binary.BigEndian)
	//log.Printf("sha256_go input: %.8x\n", bytes)
	//log.Printf("sha256_go input: %s\n", hex.EncodeToString(bytes))
	if err != nil { return nil, err }

	hash := sha256.NewUnpaddedFromMidstate(midstate)
	hash.Write(bytes)

	return as_uint32_slice(hash.Sum(nil), binary.BigEndian), nil
}
