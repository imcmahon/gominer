package spi

import (
	"fmt"
)


type Spi struct {
	hw interface{}
}


func NewDummy() (*Spi, error) {
	my_spi := new(Spi)
	hw, err := Dummy()
	if err != nil {
		return my_spi, fmt.Errorf("error constructing Dummy HW: %v", err)
	}

	my_spi.hw = hw

	return my_spi, nil
}

func NewRPI() (*Spi, error) {
	my_spi := new(Spi)
	hw, err := RPI()
	if err != nil {
		return my_spi, fmt.Errorf("error constructing RPI HW: %v", err)
	}

	my_spi.hw = hw

	return my_spi, nil
}

func (sp *Spi) String() string {
	return fmt.Sprintf("Spi { hw<%T>: %s }", sp.hw, sp.hw)
}

func (sp *Spi) Reset() {
	dummy_hw, ok := sp.hw.(*DummyHw)
	if ok {
		dummy_hw.reset()
		return
	}

	rpi_hw, ok := sp.hw.(*RpiHw)
	if ok {
		rpi_hw.reset()
		return
	}

	bbb_hw, ok := sp.hw.(*BbbHw)
	if ok {
		bbb_hw.reset()
		return
	}
}

func (sp *Spi) spi_program() {
	// send break

	// for each chip
	//		set speed to minimal
	//		set step clock
	//		set fast clock
	//		set clock division
	//		set slow clock
	//		set oclock
	// 		scan chain disable 7-11
	//		program counters
	//		padding stuff
	//		
}

func (sp *Spi) ChipInit() {
	// spi_init
	// spi program
	// reset
	// spitalk thread start

}

func (sp *Spi) spi_emit_data(addr uint16, buf []uint32) error {
	uint32 
	if len(buf) < 1 || len(buf) > 32 {
		return fmt.Errorf("spi_emit_data buf must be between 4 and 128 bytes")
	}

	return nil
}